#!/bin/bash

rm -rf $CATALINA_HOME/webapps/*
unzip /tmp/ushort-0.1.war -d $CATALINA_HOME/webapps/ROOT
if [ -z "$configFile" ]; then
    configFile = ushort-config.groovy.dev
fi
mv $CATALINA_HOME/webapps/ROOT/WEB-INF/conf/$configFile $CATALINA_HOME/webapps/ROOT/WEB-INF/conf/ushort-config.groovy

$CATALINA_HOME/bin/catalina.sh run