FROM tomcat:7-jre7
MAINTAINER Michael Astreiko <michael.astreiko@realizeideas.net>

# Copy app to /temp directory
ADD target/ushort-0.1.war /tmp/ushort-0.1.war

ADD configure.sh configure.sh
RUN cat configure.sh | tr -d '\r' > configure_clean.sh
RUN chmod +x configure_clean.sh

CMD ./configure_clean.sh